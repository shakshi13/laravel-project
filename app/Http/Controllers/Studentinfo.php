<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Studentinfo extends Controller
{
    public function home(){
        
        $text = [
            'Welcome',
            'Here We Go',
            'Now.....'
        ];
        return view('welcome', [
            'text' => $text,
            'p' => 'Page'
            ]);
    }
    public function contact(){
        return view('student.contact');
    }
    public function reg(){
        return view('student.register');
    
    }
    public function form(){
        return view('student.form');
    }
    
}
