@extends('layout')

@section('content')

<div class="container">
        @if(isset($details))
        <p> The Search results for your query <b> {{ $query }} </b> are :</p>
<form action="" method="post">
       @csrf
        <div class="input-group">
          <input type="text" class="form-control" name="q" placeholder="Search users"> 
            <span class="input-group-btn">
              <button type="submit" class="btn btn-default">
                <span class="glyphicon glyphicon-search">S</span>
              </button>
            </span>
        </div><br><br><br>
    </form>
  <p>Users Table</p>            
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>S.No</th>
        <th>Name</th>
        <th>Email</th>
        <th>Password</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
    @foreach($details as $user) 
      <tr>
        <td>{{$user->id}}</td>
        <td>{{$user->name}}</td>
        <td>{{$user->email}}</td>
        <td>{{$user->password}}</td>
        <td>
            <a href="/edit/{{$user->id}}">Edit</a>
            <a href="/destroy/{{$user->id}}">Delete</a>
        </td>
      </tr>
      
    @endforeach
    </tbody>
  </table>
  @endif
</div>


@endsection



