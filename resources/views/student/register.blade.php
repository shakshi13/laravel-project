@extends('layout')

@section('content')
	<div class="container" style="padding-top:40px">
		<form action="/store" method="post">
			@csrf
			<div class="form-group">
				<label class="col-md-4 control-label">Name</label> 
				<input type="text" class="form-control" placeholder="Name" name="name">
			<div>
			<div class="form-group">
				<label for="email">Email</label>
				<input type="email" class="form-control" placeholder="Email" name="email">
			</div>
			<div class="form-group">
				<label for="pwd">Password</label>
				<input type="password" class="form-control" placeholder="Password" name="password">
			</div>
			<!-- <div class="form-group form-check">
				<label class="form-check-label">
					<input class="form-check-input" type="checkbox" name="remember"> Remember me
				</label>
			</div> -->
			<button type="submit" class="btn btn-primary">Submit</button>
  		</form>
	</div>
	
	@endsection