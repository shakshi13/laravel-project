@extends('layout')

@section('content')

<div class="container">
  <p>Users Table</p>            
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Id</th>
        <th>Users_Id</th>
        <th>Std</th>
        <th>Country Name</th>
        <th>Contact</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
    @foreach($users as $u)
      <tr>
        <td>{{$u->id}}</td>
        <td>{{$u->users_id}}</td>
        <td>{{$u->std}}</td>
        <td>{{$u->cname}}</td>
        <td>{{$u->contact}}</td>
        <td>
            <a href="/edit/{{$u->id}}">Edit</a>
            <a href="/destroy/{{$u->id}}">Delete</a>
        </td>
      </tr>
      
    @endforeach
    </tbody>
  </table>
</div>


@endsection



