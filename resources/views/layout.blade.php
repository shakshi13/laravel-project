<!doctype html>
<html lang="en">
	<head>
		<title>@yield('title', 'Welcome')</title>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    </head>
	<body>


    <ul>
    <li><a href="/register">Register</a></li>
    <li><a href="/contact">Contact</a></li>
	<li><a href="/form">Login</a></li>
	<li><a href="/index">Table</a></li>
	<li><a href="/search">Search</a></li>
    </ul>
    
	@yield('content')
	</body>
</html>