@extends('layout')

@section('title', 'Contact Us')

@section('content')

    <section id="cover">
        <div id="container" class="container">
                <div class="col-sm-6 offset-sm-3 text-center">
                  
                    <div class="info-form">
                        <form action="" class="form-inlin justify-content-center">
                            <div class="form-group">
                                <label class="sr-only">Name</label>
                                <input type="text" class="form-control" placeholder="Enter Name">
                            </div>
                            <div class="form-group">
                                <label class="sr-only">Email</label>
                                <input type="text" class="form-control" placeholder="Enter Email">
                            </div>
                            <div class="form-group">
                                <label class="sr-only">Contact</label>
                                <input type="text" class="form-control" placeholder="Enter Contact">
                            </div>
                            <div class="form-group">
                                <label class="sr-only">Address</label>
                                <input type="text" class="form-control" placeholder="Enter Address">
                            </div>
                            <button type="submit" class="btn btn-success ">save</button>
                        </form>
                    </div><br>
                    <a href="#" class="btn btn-outline-secondary btn-sm" role="button">More </a>
                </div>
            </div>
    </section>
	
    @endsection