<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Country extends Model
{
    protected $table = "country";
    // protected $table = "users";

    public function user()
    {
        return $this->belongsTo('App\User','users_id');
    }
}
