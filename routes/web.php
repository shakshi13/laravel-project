<?php

use GuzzleHttp\Psr7\Request;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'Studentinfo@home');

Route::get('/register', 'Studentinfo@reg');

Route::get('/contact', 'Studentinfo@contact');

Route::get('/form', 'Studentinfo@form');
Route::get('/edit/{id}', 'Student@edit');
Route::post('/update/{id}', 'Student@update');
Route::get('/destroy/{id}', 'Student@destroy');

Route::post('/store', 'Student@store');
Route::get('/index', 'Student@index');

Route::any('/search','Student@search');

Route::get('/country', function(){
    $user = App\User::latest()->first();
    return $user->country;
});
Route::get('/test', function(){
    // $user = App\User::latest()->first();
    $user = App\Country::all();
    foreach($user as $u){
        echo $u->user;
        echo "<br>";
    }
    // return $user->country->std;    
});

